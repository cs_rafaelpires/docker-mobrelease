# -*- coding: utf-8 -*-

from django.conf import settings

def google_analytics(request):
    """
    Use the variables returned in this function to
    render your Google Analytics tracking code template.
    """
    ga_prop_id = getattr(settings, 'GOOGLE_ANALYTICS_PROPERTY_ID', False)
    ga_domain = getattr(settings, 'GOOGLE_ANALYTICS_DOMAIN', False)

    return {
        'GOOGLE_ANALYTICS_PROPERTY_ID': ga_prop_id,
        'GOOGLE_ANALYTICS_DOMAIN': ga_domain,
    }