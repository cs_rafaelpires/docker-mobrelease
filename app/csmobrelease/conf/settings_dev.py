# -*- coding: utf-8 -*-
# Django settings for csmobrelease project.

from conf.settings_base import *

DEBUG = True
TEMPLATE_DEBUG = DEBUG
GOOGLE_ANALYTICS_PROPERTY_ID = 'UA-532914-16'
GOOGLE_ANALYTICS_DOMAIN = None
    
ADMINS = (
    ('Jean Monteiro', 'jean.monteiro@concretesolutions.com.br'),
)

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql', # Add 'postgresql_psycopg2', 'mysql', 'sqlite3' or 'oracle'.
        'NAME': 'cs_mobrelease',              # Or path to database file if using sqlite3.
        'USER': 'root',                       # Not used with sqlite3.
        'PASSWORD': '1234',                       # Not used with sqlite3.
        'HOST': 'localhost',                  # Set to empty string for localhost. Not used with sqlite3.
        'PORT': '',                           # Set to empty string for default. Not used with sqlite3.
    }
}
