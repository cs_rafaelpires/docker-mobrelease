Docker files para o MobRelease
==============================

Para subir o MobRelease em uma máquina Linux será necessário gerar dois conteiners: um para o banco de dados e outro para a aplicação.

Banco de Dados
--------------
Um container oficial do MySQL é suficiente para a aplicação.
Para trazer a imagem do container, execute:

`docker pull mysql:latest`

Para executar o container de banco, execute:

`docker run --name mysql -e MYSQL_ROOT_PASSWORD=Concrete123 mysql:latest`

Aplicação
---------
A aplicação depende do container do banco de dados. Vamos criar um link entre elas durante a execução:

`docker run --name mobrelease --link mysql:mysql -p 80:80 wesleyit/cs-mobrelease:0.1`

Melhorias
---------
Esta é a primeira versão do container. 
Algumas melhorias mapeadas:

 - Automatizar o diretório de volume de persistência do banco e do mobrelease;
 - Criar um instalador que constrói i container e executa os serviços;
 - Criar um Docker Registry da Concrete Solutions para hospedar a imagem pronta;
 - Criar o diretório para subir o certificado HTTPS para iOS e incluir SSL na conf do apache.

Lembre-se: **Filosofia Red > Green > Refactor.**


