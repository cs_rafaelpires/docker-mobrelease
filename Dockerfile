############################################################
# MOBRELEASE
# Concrete Solutions App Store
############################################################

# Set the base image
FROM debian:jessie

# File Author / Maintainer
MAINTAINER Wesley Rodrigues da Silva <wesley.it@gmail.com>

# Set an environment variable
ENV REFRESHED_AT "28/05/2015-20:30"

# Change the container directory
WORKDIR /

# Update APT and install necessary packages
RUN apt-get update -y
RUN apt-get install -y \
	python2.7			\
	python-pip		\
	git						\
	mysql-client	\
	libmysqlclient-dev \
	python-dev		\
	apache2				\
	libapache2-mod-wsgi

# Enable the apache modules and send the main config file
RUN a2enmod proxy proxy_* wsgi
ADD mobrelease.conf /etc/apache2/sites-enabled/mobrelease.conf

# Clone mobrelease from my forked git repo
RUN git clone https://cs_wesleysilva@bitbucket.org/cs_wesleysilva/docker-mobrelease.git

# Change the container directory to app dir
WORKDIR docker-mobrelease/

# Change to devel branch and build the dependencies
RUN bash setup-project.sh 

# Allow access to mobrelease directory externaly
RUN chmod -R 775 /docker-mobrelease
RUN chown -R www-data:www-data /docker-mobrelease
RUN mkdir /data
RUN chmod -R 775 /data 
VOLUME /docker-mobrelease

# Expose HTTP and HTTPS ports
EXPOSE 80
EXPOSE 443

# Run the Django migrations and start Apache2
ENTRYPOINT ["/bin/bash"]
CMD ["/docker-mobrelease/start-mobrelease.sh"]

